#include "kms_window_system.h"
#include "atomic_kms_window_system.h"
#include "log.h"

#include <vector>
#include <system_error>
#include <thread>
#include <memory>
#include <string>
#include <csignal>
#include <algorithm>

sig_atomic_t running = true;

void sighandler(int)
{
    running = false;
}

void set_up_sighandler()
{
    struct sigaction sa{};
    sa.sa_handler = sighandler;

    sigaction(SIGTERM, &sa, nullptr);
    sigaction(SIGINT, &sa, nullptr);
}

int main(int argc, char** argv)
{
    set_up_sighandler();
    Log::init("drm_cursor", true);
    std::string argv1 = argc > 1 ? argv[1] : "";
    bool use_atomic_async = argv1 == "--atomic-async";
    bool use_atomic = use_atomic_async || argv1 == "--atomic";

    std::unique_ptr<KMSWindowSystem> kms = 
        use_atomic ?
        std::make_unique<AtomicKMSWindowSystem>("/dev/dri/card0", use_atomic_async) :
        std::make_unique<KMSWindowSystem>("/dev/dri/card0");

    auto gbm_bo = kms->create_cursor_gbm_bo(64, 64);

    std::vector<uint8_t> white(64 * gbm_bo_get_stride(gbm_bo), 0xff);
    auto ret = gbm_bo_write(gbm_bo, white.data(), white.size());
    if (ret < 0)
        throw std::system_error{-ret, std::system_category(), "Failed to gbm_bo_write"};

    kms->set_cursor(gbm_bo);

    uint32_t limit = std::min(kms->width(), kms->height());
    for (uint32_t i = 0; i < limit && running; ++i)
    {
        kms->move_cursor(i, i);
        std::this_thread::sleep_for(std::chrono::milliseconds{30});
    }
}
