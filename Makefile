CFLAGS=$(shell pkg-config --cflags libdrm gbm)
LIBS=$(shell pkg-config --libs libdrm gbm)

drm_cursor: drm_cursor.cpp \
			kms_window_system.cpp \
			atomic_kms_window_system.cpp \
			log.cpp
	g++ -std=c++14 -g -O2 ${CFLAGS} -pthread -o $@ $^ ${LIBS}

.PHONY clean:
	- rm drm_cursor
